Running the app
--  
Its a simple Spring Boot Application, can be imported into Intellij (or any other IDE)
and run-in-place. May require some settings for *Lombok* plugin.  
Maven is used for dependency management, thus - **mvn clean package spring-boot:repackage** would build the application and create an executable jar file in the target directory

The app starts at port 8080

API
--
Display org hierarchy in HTML format :   http://localhost:8080/momenton/org-hierarchy


Technical Details
---

**Main Components**

OrgHierarchyController.java  
OrgHierarchyFacade.java     
OrgHierarchyService.java        
OrgHierarchyGenerator.java      
Node.java

**Unit Test**

OrgHierarchyGeneratorTest.java      
OrgHierarchyFacadeTest.java     
NodeTest.java






