package com.momenton.controller;

import com.momenton.facade.OrgHierarchyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrgHierarchyController {

    private final OrgHierarchyFacade facade;

    @Autowired
    public OrgHierarchyController(OrgHierarchyFacade facade) {
        this.facade = facade;
    }

    @GetMapping(
        value = "/org-hierarchy",
        produces = MediaType.TEXT_HTML_VALUE
    )
    public String getOrgHierarchy() {
        return facade.getOrgHierarchy(OrgHierarchyFacade.Format.HTML);
    }
}
