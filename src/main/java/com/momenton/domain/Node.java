package com.momenton.domain;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;

/**
 * Represents non-binary tree data structure
 */
@Getter
@ToString
public class Node {
    private EmployeeModel employee;
    private List<Node> children = new ArrayList<>();

    public Node(EmployeeModel employee) {
        this.employee = employee;
    }

    private static int height(Node node) {
        int thisNodeHeight = 0;
        for (Node eachChild : emptyIfNull(node.children)) {
            int childNodeHeight = height(eachChild);
            if (childNodeHeight >= thisNodeHeight) {
                thisNodeHeight = 1 + childNodeHeight;
            }
        }
        return thisNodeHeight;
    }

    public boolean isRoot() {
        return Objects.isNull(employee.getManagerId());
    }

    public void addChild(Node node) {
        children.add(node);
    }

    public void addChildren(List<Node> nodes) {
        children.addAll(nodes);
    }

    public EmployeeModel getEmployee() {
        return employee;
    }

    public int getHeight() {
        return height(this);
    }

    public String getEmployeeName() {
        return employee.getName();
    }

    public Integer getEmployeeId() {
        return employee.getId();
    }

    public Integer getManagerId() {
        return employee.getManagerId();
    }
}
