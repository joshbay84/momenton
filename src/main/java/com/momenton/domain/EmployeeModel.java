package com.momenton.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@Builder
@EqualsAndHashCode
@Getter
public class EmployeeModel {
    private String name;
    private Integer id;
    private Integer managerId;
}
