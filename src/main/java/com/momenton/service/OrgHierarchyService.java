package com.momenton.service;

import com.momenton.domain.EmployeeModel;
import com.momenton.domain.Node;
import com.momenton.repository.EmployeeRepository;
import com.momenton.service.helper.OrgHierarchyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrgHierarchyService {

    private final EmployeeRepository repository;

    @Autowired
    public OrgHierarchyService(EmployeeRepository repository) {
        this.repository = repository;
    }

    public Node getOrgHierarchy() {
        EmployeeModel[] employees = repository.getAllEmployees();
        OrgHierarchyGenerator hierarchyGenerator = new OrgHierarchyGenerator(employees);
        return hierarchyGenerator.generate();
    }

}
