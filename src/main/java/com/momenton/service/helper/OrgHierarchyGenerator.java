package com.momenton.service.helper;

import com.momenton.domain.EmployeeModel;
import com.momenton.domain.Node;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Given employees, builds non-binary organization tree.
 * <p>
 * Not the most efficient algorithm, but ok for small list (which was mentioned in the code challenge)
 */
public class OrgHierarchyGenerator {

    private List<Node> nodes = new ArrayList<>();
    private EmployeeModel[] employees;

    public OrgHierarchyGenerator(EmployeeModel[] employees) {
        assert (ArrayUtils.isNotEmpty(employees));
        this.employees = employees;
    }

    public Node generate() {
        Arrays.stream(employees).forEach(e -> {
            Node node = new Node(e);
            associateWithParent(node);
            associateWithChildren(node);
            addNode(node);
        });

        return getRootNode();
    }

    private Node getRootNode() {
        return nodes.stream().filter(Node::isRoot).findFirst().get();
    }

    private void associateWithChildren(Node node) {
        List<Node> children = nodes.stream().filter(n -> node.getEmployeeId().equals(n.getManagerId())).collect(Collectors.toList());
        node.addChildren(children);
    }

    private void associateWithParent(Node node) {
        Optional<Node> parent = nodes.stream().filter(n -> n.getEmployeeId().equals(node.getManagerId())).findFirst();
        if (parent.isPresent()) {
            parent.get().addChild(node);
        }
    }

    private void addNode(Node node) {
        nodes.add(node);
    }

}
