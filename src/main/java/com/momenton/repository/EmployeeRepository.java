package com.momenton.repository;

import com.momenton.domain.EmployeeModel;
import org.springframework.stereotype.Repository;

/**
 * Represents in-memory data store
 */
@Repository
public class EmployeeRepository {

    private final EmployeeModel[] employees;

    public EmployeeRepository() {
        EmployeeModel alan = EmployeeModel.builder().id(100).name("Alan").managerId(150).build();
        EmployeeModel martin = EmployeeModel.builder().id(220).name("Martin").managerId(100).build();
        EmployeeModel jamie = EmployeeModel.builder().id(150).name("Jamie").build();
        EmployeeModel alex = EmployeeModel.builder().id(275).name("Alex").managerId(100).build();
        EmployeeModel steve = EmployeeModel.builder().id(400).name("Steve").managerId(150).build();
        EmployeeModel david = EmployeeModel.builder().id(190).name("David").managerId(400).build();

        employees = new EmployeeModel[]{alan, martin, jamie, alex, steve, david};
    }

    public EmployeeModel[] getAllEmployees() {
        return employees;
    }


}
