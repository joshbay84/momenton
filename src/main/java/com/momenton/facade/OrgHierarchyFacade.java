package com.momenton.facade;

import com.momenton.domain.Node;
import com.momenton.service.OrgHierarchyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrgHierarchyFacade {

    private OrgHierarchyService service;

    @Autowired
    public OrgHierarchyFacade(OrgHierarchyService service) {
        this.service = service;
    }

    public String getOrgHierarchy(Format format) {
        Node node = service.getOrgHierarchy();

        return Formatter.of(format).render(node);
    }

    public enum Format {
        HTML,
        XML
    }

    private static abstract class Formatter {
        static Formatter of(Format format) {
            switch (format) {
                case HTML:
                    return new HtmlFormatter();
                case XML:
                    return new XmlFormatter();
                default:
                    throw new IllegalArgumentException(("No formatter found for : " + format));
            }
        }

        abstract String render(Node node);
    }

    private static class HtmlFormatter extends Formatter {

        static void renderHtmlTree(Node node, int rootNodeHeight, StringBuilder sb) {
            sb.append(OldSchoolHtmlTable.rowStart());
            for (int i = 0; i <= rootNodeHeight; i++) {
                if (i == rootNodeHeight - node.getHeight()) {
                    sb.append(OldSchoolHtmlTable.columnWith(node.getEmployeeName()));
                } else {
                    sb.append(OldSchoolHtmlTable.emptyColumn());
                }
            }
            sb.append(OldSchoolHtmlTable.rowEnd());

            for (Node eachChild : node.getChildren()) {
                renderHtmlTree(eachChild, rootNodeHeight, sb);
            }
        }

        @Override
        public String render(Node node) {
            StringBuilder result = new StringBuilder(OldSchoolHtmlTable.start());

            renderHtmlTree(node, node.getHeight(), result);

            return result.append(OldSchoolHtmlTable.end()).toString();
        }

        static class OldSchoolHtmlTable {

            static String start() {
                return "<table width=\"80%;\"  border=\"1\">";
            }

            static String end() {
                return "</table>";
            }

            static String columnWith(String data) {
                return "<td>" + data + "</td>";
            }

            static String emptyColumn() {
                return "<td></td>";
            }

            static String rowStart() {
                return "<tr>";
            }

            static String rowEnd() {
                return "</tr>";
            }
        }
    }

    private static class XmlFormatter extends Formatter {
        @Override
        public String render(Node node) {
            throw new UnsupportedOperationException("Not yet supported!");
        }
    }

}
