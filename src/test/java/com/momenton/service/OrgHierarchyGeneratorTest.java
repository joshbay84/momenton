package com.momenton.service;

import com.momenton.domain.EmployeeModel;
import com.momenton.domain.Node;
import com.momenton.service.helper.OrgHierarchyGenerator;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import static com.momenton.domain.com.momenton.util.TestData.alan;
import static com.momenton.domain.com.momenton.util.TestData.alex;
import static com.momenton.domain.com.momenton.util.TestData.david;
import static com.momenton.domain.com.momenton.util.TestData.jamie;
import static com.momenton.domain.com.momenton.util.TestData.martin;
import static com.momenton.domain.com.momenton.util.TestData.steve;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OrgHierarchyGeneratorTest {

    @Test
    public void shouldGenerateOrgHierarchy() {

        EmployeeModel[] allEmployees = new EmployeeModel[]{alan, martin, jamie, alex, steve, david};

        OrgHierarchyGenerator hierarchyGenerator = new OrgHierarchyGenerator(allEmployees);

        Node rootNode = hierarchyGenerator.generate();

        assertEquals(jamie, rootNode.getEmployee());

        assertEquals(2, rootNode.getChildren().size());
        assertEquals(alan, rootNode.getChildren().get(0).getEmployee());
        assertEquals(steve, rootNode.getChildren().get(1).getEmployee());

        assertEquals(2, rootNode.getChildren().get(0).getChildren().size());
        assertEquals(martin, rootNode.getChildren().get(0).getChildren().get(0).getEmployee());
        assertEquals(alex, rootNode.getChildren().get(0).getChildren().get(1).getEmployee());

        assertEquals(1, rootNode.getChildren().get(1).getChildren().size());
        assertEquals(david, rootNode.getChildren().get(1).getChildren().get(0).getEmployee());

    }

    @Test
    public void shouldExcludeEmployeesWithInvalidManagersFromHierarchy() {
        EmployeeModel alex_invalid = EmployeeModel.builder().id(275).name("Alex").managerId(1000000).build();
        EmployeeModel david_invalid = EmployeeModel.builder().id(190).name("David").managerId(1000000).build();

        EmployeeModel[] allEmployees = new EmployeeModel[]{alan, martin, jamie, alex_invalid, steve, david_invalid};

        OrgHierarchyGenerator hierarchyGenerator = new OrgHierarchyGenerator(allEmployees);
        Node rootNode = hierarchyGenerator.generate();

        assertEquals(jamie, rootNode.getEmployee());

        assertEquals(2, rootNode.getChildren().size());
        assertEquals(alan, rootNode.getChildren().get(0).getEmployee());
        assertEquals(steve, rootNode.getChildren().get(1).getEmployee());

        assertEquals(1, rootNode.getChildren().get(0).getChildren().size());
        assertEquals(martin, rootNode.getChildren().get(0).getChildren().get(0).getEmployee());

        assertTrue(CollectionUtils.isEmpty(rootNode.getChildren().get(1).getChildren()));

    }
}
