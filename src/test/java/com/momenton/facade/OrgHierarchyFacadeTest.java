package com.momenton.facade;

import com.momenton.domain.com.momenton.util.TestData;
import com.momenton.service.OrgHierarchyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrgHierarchyFacadeTest {

    private OrgHierarchyFacade testInstance;
    @Mock
    private OrgHierarchyService service;

    @Before
    public void before() {
        testInstance = new OrgHierarchyFacade(service);
    }

    @Test
    public void shouldGenerateOrgHierarchyInHTML() {
        String expected = "<table width=\"80%;\"  border=\"1\">" +
            "<tr><td>Jamie</td><td></td><td></td></tr>" +
            "<tr><td></td><td>Alan</td><td></td></tr>" +
            "<tr><td></td><td></td><td>Martin</td></tr>" +
            "<tr><td></td><td></td><td>Alex</td></tr>" +
            "<tr><td></td><td>Steve</td><td></td></tr>" +
            "<tr><td></td><td></td><td>David</td></tr>" +
            "</table>";

        when(service.getOrgHierarchy()).thenReturn(TestData.jamieNode);

        String result = testInstance.getOrgHierarchy(OrgHierarchyFacade.Format.HTML);

        assertEquals(expected, result);
    }
}
