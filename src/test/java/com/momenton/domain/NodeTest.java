package com.momenton.domain;

import com.momenton.domain.com.momenton.util.TestData;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NodeTest {

    @Test
    public void shouldCalculateHeightOfNode() {

        assertEquals(2, TestData.jamieNode.getHeight());
        assertEquals(1, TestData.alanNode.getHeight());
        assertEquals(1, TestData.steveNode.getHeight());
        assertEquals(0, TestData.martinNode.getHeight());
        assertEquals(0, TestData.alexNode.getHeight());
        assertEquals(0, TestData.davidNode.getHeight());
    }
}
