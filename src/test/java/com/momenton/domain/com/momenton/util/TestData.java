package com.momenton.domain.com.momenton.util;

import com.momenton.domain.EmployeeModel;
import com.momenton.domain.Node;

public class TestData {

    public static Node alanNode, martinNode, jamieNode, alexNode, steveNode, davidNode;
    public static EmployeeModel alan, martin, jamie, alex, steve, david;

    static {
        alan = EmployeeModel.builder().id(100).name("Alan").managerId(150).build();
        martin = EmployeeModel.builder().id(220).name("Martin").managerId(100).build();
        jamie = EmployeeModel.builder().id(150).name("Jamie").build();
        alex = EmployeeModel.builder().id(275).name("Alex").managerId(100).build();
        steve = EmployeeModel.builder().id(400).name("Steve").managerId(150).build();
        david = EmployeeModel.builder().id(190).name("David").managerId(400).build();

        jamieNode = new Node(jamie);
        alanNode = new Node(alan);
        steveNode = new Node(steve);
        martinNode = new Node(martin);
        alexNode = new Node(alex);
        davidNode = new Node(david);

        jamieNode.addChild(alanNode);
        jamieNode.addChild(steveNode);

        alanNode.addChild(martinNode);
        alanNode.addChild(alexNode);

        steveNode.addChild(davidNode);
    }

}
